Team number: xohw18-288

Project name: Embedded Multispectral Camera
Date: 30/06/2018

Version of uploaded archive: V1.0

University name: Vrije Universiteit Brussel
Supervisor name: Abdellah Touhafi
Supervisor e-mail: abdellah.touhafi@vub.be

Participant: Tom van Rijn
Email: tom.van.rijn@vub.be

Participant: Jurgen Vandendriessche
Email: jurgen.vandendriessche@vub.be

Participant: Robin Verbruggen
Email: robin.verbruggen@vub.be

Board used: Zedboard

Vivado Version: Vivado 2016.4

Brief description of project: 

The project consists of two parts, an infrared part using a Grid-EYE and an RGB part OV7670 (we use the DV7670 development board). The two images captured by those sensors can be combined or displayed separately. The combination of the IR and RGB images as well as the application of a filter on the RGB image is done by using the switches.

NOTE: The Vivado HLS projects that were used to create the filters can be found in the RGB.zip archive, in the 'c' folder located in the 'src' folder.

Description of archive :

IRGB:
Step 1: Connect a VGA cable to the Zedboard
Step 2: Connect the OV7670 camera to the PMOD pins according to the constraint file
Step 3: Connect the Grid-EYE infrared sensor to the PMOD pins according to the constraint file
Step 4: Open Vivado 2016.4
Step 5: Go to the <IRGB> folder
Step 6: Select <IRGB.xpr>
Step 7: Once the project is open: Open the hardware manager
Step 8: Program the FPGA using the .bit file in the <hw> folder
Step 9: The RGB and/or IR images should be displayed, according to how the switches are set

RGB:
Step 1: Connect a VGA cable to the Zedboard
Step 2: Connect the OV7670 camera to the PMOD pins according to the constraint file
Step 3: Connect the Grid-EYE infrared sensor to the PMOD pins according to the constraint file
Step 4: Open Vivado 2016.4
Step 5: Go to the <RGB> folder
Step 6: Select <IRGB.xpr>
Step 7: Once the project is open: Open the hardware manager
Step 8: Program the FPGA using the .bit file in the <hw> folder
Step 9: The RGB images should be displayed and/or filtered, according to how the switches are set

IR:
Step 1: Connect a VGA cable to the Zedboard
Step 2: Connect the OV7670 camera to the PMOD pins according to the constraint file
Step 3: Connect the Grid-EYE infrared sensor to the PMOD pins according to the constraint file
Step 4: Open Vivado 2016.4
Step 5: Go to the <IR> folder
Step 6: Select <IRGB.xpr>
Step 7: Once the project is open: Open the hardware manager
Step 8: Program the FPGA using the .bit file in the <hw> folder
Step 9: The IR images should be displayed

Link to YouTube Video(s): https://youtu.be/EUSeBYTH974
